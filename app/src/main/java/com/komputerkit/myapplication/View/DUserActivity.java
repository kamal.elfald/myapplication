package com.komputerkit.myapplication.View;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.komputerkit.myapplication.R;
import com.komputerkit.myapplication.Sql.DatabaseHelper;
import com.komputerkit.myapplication.Model.User;
import com.komputerkit.myapplication.utils.MyDividerItemDecoration;
import com.komputerkit.myapplication.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class DUserActivity extends AppCompatActivity {
    private DatabaseHelper db;
    private DataUserAdapter mAdapter;
    private List<User> userList = new ArrayList<>();

    private RecyclerView recyclerView;
    private TextView noUserView;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duser);
        noUserView = (TextView) findViewById(R.id.noUserView);
        final RecyclerView recyclerView = findViewById(R.id.recycler_user);
        db = new DatabaseHelper(this);

        userList.addAll(db.getAllUsers());

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUserDialog(false, null, -1);
            }
        });

        mAdapter = new DataUserAdapter(this, userList);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this,
                LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);
        toggleEmptyUser();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                showActionDialog(position);
            }
        }));
    }



    private void showActionDialog(final int position) {
        CharSequence color[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(color, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (which == 0) {
                    showUserDialog(true,
                            userList.get(position), position);

                } else {
                    deleteUser(position);
                }
            }
        });
        builder.show();
    }

    private void deleteUser(int position) {
        db.deleteUser(userList.get(position));
        userList.remove(position);
        mAdapter.notifyItemRemoved(position);
        toggleEmptyUser();
    }

    private void createUser(String username, String password, String email) {
        long id = db.insertUser(username, password, email);
        User u = db.getUser(id);
        if (u != null) {
            userList.add(0, u);
            mAdapter.notifyDataSetChanged();
            toggleEmptyUser();
        }
    }

    private void updateUser(String username, String password, String email, int position) {
        User u = userList.get(position);
        u.setUsername(username);
        u.setEmail(email);
        u.setPassword(password);
        db.updateUser(u);
        userList.set(position, u);
        mAdapter.notifyItemChanged(position);
        toggleEmptyUser();
    }

    private void showUserDialog(final boolean shouldUpdate,
                                final User user, final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.
                from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.user_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput =
                new AlertDialog.Builder(DUserActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputUsername = view.findViewById(R.id.username);
        final EditText inputPassword = view.findViewById(R.id.password);
        final EditText inputEmail = view.findViewById(R.id.emailbr);

        TextView dialogTitle = view.findViewById(R.id.dialog_title);
        dialogTitle.setText(!shouldUpdate ? "User Baru" : "Edit Note");

        if (shouldUpdate && user != null) {
            inputUsername.setText(user.getUsername());
            inputPassword.setText(user.getPassword());
            inputEmail.setText(user.getEmail());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "Update" : "Save",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });
        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // show toast message when no text is entered
                        if (TextUtils.isEmpty(inputUsername.getText().toString())) {
                            Toast.makeText(DUserActivity.this,
                                    "Enter Nama User!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputPassword.getText().toString())) {
                            Toast.makeText(DUserActivity.this,
                                    "Enter Password!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputEmail.getText().toString())) {
                            Toast.makeText(DUserActivity.this,
                                    "Enter Email!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (shouldUpdate && user != null) {
                            updateUser(inputUsername.getText().toString(),
                                    inputPassword.getText().toString(),
                                    inputEmail.getText().toString(), position);
                        } else {
                            createUser(inputUsername.getText().toString(),
                                    inputPassword.getText().toString(),
                                    inputEmail.getText().toString());
                        }

                    }
                });
    }

    @SuppressLint("RestrictedApi")
    public void toggleEmptyUser() {
        if (db.getUserCount() > 0) {
            noUserView.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);
        } else {
            noUserView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }
    }
}

