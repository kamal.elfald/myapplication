package com.komputerkit.myapplication.View;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.komputerkit.myapplication.Model.Barang;
import com.komputerkit.myapplication.R;
import com.komputerkit.myapplication.Sql.DatabaseHelper;
import com.komputerkit.myapplication.utils.MyDividerItemDecoration;
import com.komputerkit.myapplication.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class DBarangActivity extends AppCompatActivity{
    private DatabaseHelper db;
    private DataBarangAdapter mAdapter;
    private List<Barang> barangList = new ArrayList<>();

    private RecyclerView recyclerView;
    private TextView noBarangView;
    private FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbarang);
        noBarangView=(TextView)findViewById(R.id.noBarangView);
        final RecyclerView recyclerView =findViewById(R.id.recycler_barang);

        db = new DatabaseHelper(this);

        barangList.addAll(db.getAllBarang());

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                showBarangDialog(false,
                        null, -1);
            }

        });

        mAdapter = new DataBarangAdapter(this, barangList);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this,
                LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);
        toggleEmptyBarang();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener(){
            @Override
            public void onClick(View view, int position){

            }
            @Override
            public void onLongClick(View view, int position){
                showActionDialog(position);
            }
        }));
    }

    private void showActionDialog(final int position){
        CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            showBarangDialog(true,
                                    barangList.get(position), position);
                        } else {
                            deleteBarang(position);
                        }
                    }
                });
        builder.show();

    }
        private void deleteBarang(int position){
            db.deleteBarang(barangList.get(position));
            barangList.remove(position);
            mAdapter.notifyItemRemoved(position);
            toggleEmptyBarang();

        }

        private void createBarang(String nama, int harga_beli,
                            int harga_jual, int stok) {
            long id = db.insertBarang(nama,harga_beli,harga_jual,stok);
            Barang n = db.getBarang(id);
            if (n != null) {
                barangList.add(0, n);
                mAdapter.notifyDataSetChanged();
                toggleEmptyBarang();
            }
        }
        private void updateBarang(String nama,int harga_beli,int harga_jual
        , int stok, final int position) {
            Barang n = barangList.get(position);
            n.setNama_barang(nama);
            n.setHarga_beli(harga_beli);
            n.setHarga_jual(harga_jual);
            n.setStok(stok);
            db.updateBarang(n);
            barangList.set(position, n);
            mAdapter.notifyItemChanged(position);
            toggleEmptyBarang();
        }

        private void showBarangDialog(final boolean shouldUpdate,
                                    final Barang barang, final int positon){
            LayoutInflater layoutInflaterAndroid = LayoutInflater.
                    from(getApplicationContext());
            View view = layoutInflaterAndroid.inflate(R.layout.barang_dialog, null);

            AlertDialog.Builder alertDialogBuilderUserInput =
                    new AlertDialog.Builder(DBarangActivity.this);
            alertDialogBuilderUserInput.setView(view);

            final EditText inputNama = view.findViewById(R.id.nama_barang);
            final EditText inputHarga_beli = view.findViewById(R.id.harga_beli);
            final  EditText inputHarga_jual = view.findViewById(R.id.harga_jual);
            final EditText inputStok = view.findViewById(R.id.stok);

            TextView dialogTitle = view.findViewById(R.id.dialog_title);
            dialogTitle.setText(!shouldUpdate ? "Barang Baru" : "Edit Note");

            if (shouldUpdate && barang != null) {
                inputNama.setText(barang.getNama_barang());
                inputHarga_beli.setText(Integer.toString(barang.getHarga_beli()));
                inputHarga_jual.setText(Integer.toString(barang.getHarga_jual()));
                inputStok.setText(Integer.toString(barang.getStok()));
            }
            alertDialogBuilderUserInput
                    .setCancelable(false)
                    .setPositiveButton(shouldUpdate ? "update" : "save",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {

                                }
                            })
                    .setNegativeButton("cancel",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialogBox, int id){
                            dialogBox.cancel();
                        }
                    });
            final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
            alertDialog.show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).
                    setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (TextUtils.isEmpty(inputNama.getText().toString())){
                                Toast.makeText(DBarangActivity.this,
                                        "Enter Nama Barang!", Toast.LENGTH_SHORT).show();
                                return;

                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputHarga_beli.getText().toString())){
                            Toast.makeText(DBarangActivity.this,
                            "Enter Harga Beli!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputHarga_jual.getText().toString())){
                            Toast.makeText(DBarangActivity.this,
                                    "Enter Harga Jual!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputStok.getText().toString())){
                            Toast.makeText(DBarangActivity.this,
                                    "Enter Stok!", Toast.LENGTH_SHORT).show();
                            return;
                        } else{
                            alertDialog.dismiss();
                        }
                        if (shouldUpdate && barang != null){
                            updateBarang(inputNama.getText().toString(),
                                    Integer.parseInt(inputHarga_beli.getText().toString()),
                                    Integer.parseInt(inputHarga_jual.getText().toString()),
                                    Integer.parseInt(inputStok.getText().toString()), positon);
                        } else {
                            createBarang(inputNama.getText().toString(),
                                    Integer.parseInt(inputHarga_beli.getText().toString()),
                                    Integer.parseInt(inputHarga_jual.getText().toString()),
                                    Integer.parseInt(inputStok.getText().toString()));
                        }

                    }
                });
        }
        @SuppressLint("RestrictedApi")
        private void toggleEmptyBarang(){
            if (db.getBarangCount()>0){
                noBarangView.setVisibility(View.GONE);
                fab.setVisibility(View.VISIBLE);
            } else {
                noBarangView.setVisibility(View.VISIBLE);
                fab.setVisibility(View.VISIBLE);
            }
        }
    }

