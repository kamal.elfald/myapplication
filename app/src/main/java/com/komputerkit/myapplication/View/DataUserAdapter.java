package com.komputerkit.myapplication.View;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.komputerkit.myapplication.Model.User;
import com.komputerkit.myapplication.R;

import java.util.List;

public class DataUserAdapter extends RecyclerView.Adapter<DataUserAdapter
        .DataUserHolder>{
    Context context;
    List<User>Data;
    public class DataUserHolder extends RecyclerView.ViewHolder
    {
        TextView txtUser,txtEmail,dot;
        public DataUserHolder(View itemView)
        {
            super(itemView);
            txtUser = itemView.findViewById(R.id.txt_username);
            dot=itemView.findViewById(R.id.dot);
            txtEmail = itemView.findViewById(R.id.emailbr);
        }
    }
    public DataUserAdapter(Context context, List<User> data)
    {
        this.context=context;
        this.Data = data;
    }
    @NonNull
    @Override
    public DataUserHolder onCreateViewHolder(@NonNull ViewGroup parent, int
                                             viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate
                (R.layout.activity_datauser, parent, false);
        return new DataUserHolder(v);
    }
    @Override
    public  void onBindViewHolder(@NonNull DataUserAdapter.DataUserHolder holder, int
                                  position)
    {
        User user = Data.get(position);
        holder.txtUser.setText(user.getUsername());

        holder.dot.setText(Html.fromHtml("&#8226"));
        holder.txtEmail.setText(user.getEmail());
    }
    @Override

    public int getItemCount(){
        return Data.size();
    }
}
