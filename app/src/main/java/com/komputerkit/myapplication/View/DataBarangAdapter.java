package com.komputerkit.myapplication.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.komputerkit.myapplication.R;
import com.komputerkit.myapplication.Model.Barang;

import java.util.List;

public class DataBarangAdapter extends RecyclerView.
        Adapter<DataBarangAdapter.DataBarangHolder>{
    Context context;
    List<Barang>data;
    public class DataBarangHolder extends RecyclerView.ViewHolder
    {
        TextView txtNama, txtStok,dot;
        public DataBarangHolder(View itemView)
        {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtnama_barang);
            dot=itemView.findViewById(R.id.dot);
            txtStok = itemView.findViewById(R.id.stokbr);
        }
    }
    public DataBarangAdapter(Context context,List<Barang> data)
    {
        this.context=context;
        this.data=data;
    }
    @NonNull
    @Override
    public DataBarangHolder onCreateViewHolder(@NonNull ViewGroup parent, int
                                               viewType)
    {
        View v =LayoutInflater.from(parent.getContext()).inflate
                (R.layout.activity_databarang,parent, false);
        return new DataBarangHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull DataBarangHolder holder, int
                                 position)
    {
        Barang barang = data.get(position);
        holder.txtNama.setText(barang.getNama_barang());
        holder.dot.setText(Html.fromHtml("&#8226"));
        holder.txtStok.setText(Integer.toString(barang.getStok()));


    }
    @Override
    public int getItemCount() {return data.size();}
}