package com.komputerkit.myapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.komputerkit.myapplication.View.DBarangActivity;
import com.komputerkit.myapplication.View.DUserActivity;

public class Main2Activity extends AppCompatActivity {
        ImageButton btnBarang;
        ImageButton btnUser;

        @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnBarang = (ImageButton) findViewById(R.id.btnbarang);
        btnBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Main2Activity.this,
                        DBarangActivity.class));
            }
        });
        btnUser = (ImageButton) findViewById(R.id.btnuser);
        btnUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Main2Activity.this,
                        DUserActivity.class));
            }
        });
    }
}
