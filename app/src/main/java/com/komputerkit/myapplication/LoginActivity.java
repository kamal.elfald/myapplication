package com.komputerkit.myapplication;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;


import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class LoginActivity extends AppCompatActivity{
    ConstraintLayout cs1;
    Handler handler = new Handler();
    EditText tusername, tpassword;
    String username,password;
    Button btnmasuk,btnsigup,btnlupa;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            cs1.setVisibility(View.VISIBLE);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cs1 = (ConstraintLayout)findViewById(R.id.cs1);
        handler.postDelayed(runnable, 500);
        tusername=(EditText)findViewById(R.id.tusername);
        tpassword=(EditText)findViewById(R.id.tpassword);
        btnmasuk=(Button)findViewById(R.id.btnmasuk);
        btnmasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid())
                {
                    username= tusername.getText().toString();
                    password= tpassword.getText().toString();
                    login(username,password);
                }
            }
        });
    }

    private boolean isValid() {
        if (tusername.getText().toString().equals("")) {
            tusername.setError("Username kosong");
            return false;
        }
        if (tpassword.getText().toString().equals(" ")) {
            tpassword.setError("Password tidak boleh kosong");
            return false;
        }
        return true;
    }
        private void login (String username,String password){
            if (username.equals("admin") && (password.equals("1234")))
            {
                showAlert("Selamat","anda benar");
            }
            else
            {
                showAlertTutup();
            }
        }
        private void showAlertTutup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage("Tutup Aplikasi Ini")
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            finish();
                        }
                    });
            Dialog dialog= builder.create();
            dialog.show();
        }
        private void showAlert(String title, String pesan){
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(pesan)

                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            startActivity(new Intent(LoginActivity.this,Main2Activity.class));
                        }
                    });
            AlertDialog dialog= builder.create();
            dialog.show();
        }
    }