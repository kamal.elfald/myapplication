package com.komputerkit.myapplication.Sql;

import android.content.ContentValues;
import android.content.Context;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.komputerkit.myapplication.Model.Barang;
import com.komputerkit.myapplication.Model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    //Database version
    private static final int DATABASE_VERSION = 1;

    //Database name
    private static final String DATABASE_NAME = "penjualan_db";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Barang.CREATE_TABLE);
        db.execSQL(User.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int il){
        db.execSQL("DROP TABLE IF EXISTS " +  Barang.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + User.TABLE_NAME);


        //Create table again
        onCreate(db);
    }
    public long insertBarang(String nama, int harga_beli,
                             int harga_jual, int stok){

        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();



        values.put(Barang.COLUMN_Nama_Barang, nama);
        values.put(Barang.COLUMN_harga_Beli, harga_beli);
        values.put(Barang.COLUMN_harga_jual, harga_jual);
        values.put(Barang.COLUMN_stok, stok);

        long id = db.insert(Barang.TABLE_NAME, null, values);

        //close db connection
        db.close();


        //return newly inserted row id
        return id;
    }

    public Barang getBarang(long id){

        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.query(Barang.TABLE_NAME,
                new String[]{Barang.COLUMN_ID, Barang.COLUMN_Nama_Barang,
                Barang.COLUMN_harga_Beli,
                Barang.COLUMN_harga_jual, Barang.COLUMN_stok,
                Barang.COLUMN_TIMESTAMP},
                Barang.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null,
                null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Barang barang = new Barang(
                cursor.getInt(cursor.getColumnIndex(Barang.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Barang.COLUMN_Nama_Barang)),
                cursor.getInt(cursor.getColumnIndex(Barang.COLUMN_harga_Beli)),
                cursor.getInt(cursor.getColumnIndex(Barang.COLUMN_harga_jual)),
                cursor.getInt(cursor.getColumnIndex(Barang.COLUMN_stok)),
                cursor.getString(cursor.getColumnIndex(Barang.COLUMN_TIMESTAMP)));

                cursor.close();

                return barang;
    }

    public List<Barang> getAllBarang(){
        List<Barang> notes = new ArrayList<>();


        String selectQuery = "SELECT * FROM " +  Barang.TABLE_NAME
                + " ORDER BY " +
                Barang.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor= db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do {
                Barang barang = new Barang();
                barang.setId(cursor.getInt(cursor.
                        getColumnIndex(Barang.COLUMN_ID)));
                barang.setNama_barang(cursor.getString(cursor.
                        getColumnIndex(Barang.COLUMN_Nama_Barang)));
                barang.setHarga_beli(cursor.getInt(cursor
                    .getColumnIndex(Barang.COLUMN_harga_Beli)));
                barang.setHarga_jual(cursor.getInt(cursor.
                        getColumnIndex(Barang.COLUMN_harga_jual)));
                barang.setStok(cursor.getInt(cursor.
                        getColumnIndex(Barang.COLUMN_stok)));
                barang.setTimestamp(cursor.getString(cursor.
                        getColumnIndex(Barang.COLUMN_TIMESTAMP)));

                notes.add(barang);
            } while (cursor.moveToNext());
        }

        //close db connection
        db.close();

        //return notes list
        return notes;
    }

    public int getBarangCount(){
        String countQuery = "SELECT * FROM " + Barang.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        //return count
        return count;
    }

    public int updateBarang(Barang barang){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues  values = new ContentValues();
        values.put(Barang.COLUMN_Nama_Barang, barang.getNama_barang());
        values.put(Barang.COLUMN_harga_Beli, barang.getHarga_beli());
        values.put(Barang.COLUMN_harga_jual, barang.getHarga_jual());
        values.put(Barang.COLUMN_stok, barang.getStok());
        ContentValues values1 = new ContentValues();
        values.put(Barang.COLUMN_Nama_Barang, barang.getNama_barang());
        values.put(Barang.COLUMN_harga_Beli, barang.getHarga_beli());
        values.put(Barang.COLUMN_harga_jual, barang.getHarga_jual());
        values.put(Barang.COLUMN_stok, barang.getStok());


        return db.update(Barang.TABLE_NAME, values,
                Barang.COLUMN_ID + " = ?",
                new String[]{String.valueOf(barang.getId())});
    }

    public void deleteBarang(Barang barang) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Barang.TABLE_NAME, Barang.COLUMN_ID + " = ?",
                new String[]{String.valueOf(barang.getId())});
        db.close();
    }
    // user

    public long insertUser(String username, String password, String email) {
        // get writeable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // id and timestamps will be inserted automatically.
        // no nedd to add thme;
        values.put(User.COLUMN_username, username);
        values.put(User.COLUMN_password, password);
        values.put(User.COLUMN_email, email);

        long id = db.insert(User.TABLE_NAME, null, values);

        // close db connect
        db.close();

        // return newly inserted row id
        return id;
    }

    public User getUser(long id ) {
        // get readable db as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor =  db.query(User.TABLE_NAME,
                new String[]{User.COLUMN_ID, User.COLUMN_username,
                        User.COLUMN_password,
                        User.COLUMN_email},
                User.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null,
                null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        // preapre note object
        User user = new User(
                cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_username)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_password)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_email)));

        // close the db connect
        cursor.close();

        return user;
    }

    public List<User> getAllUsers(){
        List<User> notes = new ArrayList<>();

        //select all query
        String selectQuery = "SELECT * FROM " + User.TABLE_NAME + " ORDER BY " + User.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        //lopping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do{
                User user = new User();
                user.setId(cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID)));
                user.setUsername(cursor.getString(cursor.getColumnIndex(User.COLUMN_username)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(User.COLUMN_password)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(User.COLUMN_email)));

                notes.add(user);
            }while (cursor.moveToNext());
        }
        //close db connection
        db.close();
        return notes;
    }

    /*public List<User> getAllUsers() {
        List<User> notes = new ArrayList<>();

        // select all query
        String selectQuery = "SELECT * FROM " + User.TABLE_NAME
                + " ORDER BY " +
                User.COLUMN_ID + " DESC "; // urutkan

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(cursor.getInt(cursor.
                        getColumnIndex(User.COLUMN_ID)));
                user.setUsername(cursor.getString(cursor.
                        getColumnIndex(User.COLUMN_username)));
                user.setPassword(cursor.getString(cursor.
                        getColumnIndex(User.COLUMN_password)));
                user.setEmail(cursor.getString(cursor.
                        getColumnIndex(User.COLUMN_email)));

                notes.add(user);
            } while (cursor.moveToNext());
        }

        // close db connect
        db.close();

        // return notes list
        return notes;
    }*/

    public int getUserCount() {
        String countQuery = "SELECT * FROM " + User.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(User.COLUMN_username, user.getUsername());
        values.put(User.COLUMN_password, user.getPassword());
        values.put(User.COLUMN_email, user.getEmail());

        // updating row
        return db.update(User.TABLE_NAME, values,
                User.COLUMN_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
    }

    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(User.TABLE_NAME, User.COLUMN_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

}




