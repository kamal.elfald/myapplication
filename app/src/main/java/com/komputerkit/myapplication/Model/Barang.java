package com.komputerkit.myapplication.Model;

public class Barang {
    public static final String TABLE_NAME = "barang";


    public static final String COLUMN_ID = "id";
    public static final String COLUMN_Nama_Barang = "nama_barang";
    public static final String COLUMN_harga_Beli = "harga_beli";
    public static final String COLUMN_harga_jual = "harga_jual";
    public static final String COLUMN_stok = "stok";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    private int id;
    private String nama_barang;
    private int harga_beli;
    private int harga_jual;
    private int stok;
    private String timestamp;
    public static final String CREATE_TABLE =
            "Create Table "+ TABLE_NAME+"("
            + COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_Nama_Barang+ " TEXT, "
            + COLUMN_harga_Beli + " INTEGER ,"
            + COLUMN_harga_jual + " INTEGER ,"
            + COLUMN_stok + " INTEGER,"
            + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP "
            +")";
    public Barang ()
    {}
    public Barang(int id, String nama, int harga_beli, int harga_jual,
                  int stok, String timestamp)
    {
        this.id=id;
        this.nama_barang=nama;
        this.harga_beli=harga_beli;
        this.harga_jual=harga_jual;
        this.stok=stok;
        this.timestamp=timestamp;
    }

    public void setHarga_beli(int harga_beli) {
        this.harga_beli =  harga_beli;
    }

    public void setHarga_jual(int harga_jual){
        this.harga_jual = harga_jual;
    }

    public void setId(int id) {
        this.id = id;
    }



    public void setNama_barang(String nama_barang){
        this.nama_barang =  nama_barang;
    }
    public void setStok(int stok){
        this.stok =  stok;
    }
    public void setTimestamp(String timestamp){
        this.timestamp = timestamp;
    }
    public int getHarga_beli(){
        return harga_beli;
    }
    public int getHarga_jual(){
        return harga_jual;
    }

    public int getId() { return id; }

    public int getStok() { return  stok; }

    public String getNama_barang() { return nama_barang;}

    public String getTimestamp() { return timestamp;}
}
