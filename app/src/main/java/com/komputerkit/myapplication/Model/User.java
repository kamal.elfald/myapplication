package com.komputerkit.myapplication.Model;

public class User {
    public static final String TABLE_NAME = "user";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_username = "username";
    public static final String COLUMN_password = "password";
    public static final String COLUMN_email = "email";

    private Integer id;
    private String username;
    private String password;
    private String email;

    public static final String CREATE_TABLE = "Create Table "+ TABLE_NAME +"("
            + COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_username+ " TEXT ,"
            + COLUMN_password+ " TEXT ,"
            + COLUMN_email + " TEXT "
            +")";
    public User()
        {}

        public User(Integer id, String username, String password, String email)
        {
            this.id =  id;
            this.username = username;
            this.password = password;
            this.email = email;
        }

        public void setId(Integer id){
           this.id = id;
        }
        public Integer getId(){
           return id;
        }

        public void setUsername(String username){
           this.username = username;
        }
        public String getUsername(){
           return username;
        }

        public void setPassword(String password){
           this.password = password;
        }

        public String getPassword(){
           return password;
        }

        public void setEmail(String email){
           this.email = email;
        }

        public String getEmail(){
           return email;
        }
    }

